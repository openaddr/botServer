package cn.yn.sever.botserver.bean;

import lombok.Data;

@Data
public class BotConfig {
    private Long id;
    private String pwd;
}
